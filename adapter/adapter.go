package adapter

type PaymentMethod interface {
	Pay()
	Refund()
}
