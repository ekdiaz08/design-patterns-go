package main

import (
	"fmt"

	"gitlab.com/ekdiaz08/design-patterns-go/adapter"
)

func main() {
	var pm string
	fmt.Print("insert the payment method: ")
	fmt.Scanln(&pm)

	paymentMethod := adapter.NewFactory(pm)
	paymentMethod.Pay()

}
