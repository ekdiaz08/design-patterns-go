package adapter

import "gitlab.com/ekdiaz08/design-patterns-go/adapter/openpay"

type PaymentMethodOpenPay struct {
	paymentMethod *openpay.OpenPay
}

func (p PaymentMethodOpenPay) Pay() {
	p.paymentMethod.Pay()
}

func (p PaymentMethodOpenPay) Refund() {
	p.paymentMethod.Refund()
}
