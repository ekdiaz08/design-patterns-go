package adapter

import (
	"gitlab.com/ekdiaz08/design-patterns-go/adapter/stripe"
)

type PaymentMethodStripe struct {
	paymentMethod1 *stripe.Stripe
}

func (p PaymentMethodStripe) Pay() {
	p.paymentMethod1.Pay()
	p.paymentMethod1.Confirm()
}
func (p PaymentMethodStripe) Refund() {
	p.paymentMethod1.Refund()
}
