package adapter

import (
	"gitlab.com/ekdiaz08/design-patterns-go/adapter/openpay"
	"gitlab.com/ekdiaz08/design-patterns-go/adapter/stripe"
)

func NewFactory(paymentMethodType string) PaymentMethod {
	switch paymentMethodType {
	case "stripe":
		p := stripe.Stripe{}
		return &PaymentMethodStripe{&p}
	case "openpay":
		p := openpay.OpenPay{}
		return &PaymentMethodOpenPay{&p}
	}

	return nil
}
