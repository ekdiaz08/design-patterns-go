package openpay

import "fmt"

type OpenPay struct {
	SourceID string
	Amount   int64
}

func (o OpenPay) Pay() {
	fmt.Println("pay!!")
}

func (o OpenPay) Refund() {
	fmt.Println("refund!!")
}
