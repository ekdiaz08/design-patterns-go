package stripe

import "fmt"

type Stripe struct {
	Description string
	Amount      int64
}

func (s Stripe) Pay() {
	fmt.Println("pay!!")
}

func (s Stripe) Confirm() {
	fmt.Println("confirm!!")
}

func (s Stripe) Refund() {
	fmt.Println("refund!!")
}
